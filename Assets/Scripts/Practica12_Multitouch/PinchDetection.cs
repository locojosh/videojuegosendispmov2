using System.Collections;
using UnityEngine;

public class PinchDetection : MonoBehaviour
{
    private MobileControls controls;
    private Coroutine coroutine;
    private Transform cameraTransform;
    public float cameraSpeed = 5f;

    private void Awake() {
        controls = new MobileControls();
        cameraTransform = Camera.main.transform;
    }
    private void OnEnable() {
        controls.Enable();
    }
    private void OnDisable() {
        controls.Disable();
    }
    private void Start() {
        controls.Touch.SecondaryTouchContact.started += _ => ZoomStart();
        controls.Touch.SecondaryTouchContact.canceled += _ => ZoomEnd();
    }

    private void ZoomStart()
    {
        coroutine = StartCoroutine("ZoomCoroutine");
    }
    private void ZoomEnd()
    {
        StopCoroutine(coroutine);
    }

    IEnumerator ZoomCoroutine()
    {
        float previousDistance = 0f, distance = 0f;
        while(true)
        {
            distance = Vector2.Distance(controls.Touch.PrimaryFingerPosition.ReadValue<Vector2>(),
                        controls.Touch.SecondaryFingerPosition.ReadValue<Vector2>());
            //Detection
            //Zoom out
            if(distance > previousDistance)
            {
                Vector3 targetPosition = cameraTransform.position;
                targetPosition.z -= 1;
                cameraTransform.position = Vector3.Slerp(cameraTransform.position, 
                                                    targetPosition, Time.deltaTime * cameraSpeed);
            }
            //Zoom in
            else if(distance < previousDistance)
            {
                Vector3 targetPosition = cameraTransform.position;
                targetPosition.z += 1;
                cameraTransform.position = Vector3.Slerp(cameraTransform.position, 
                                                    targetPosition, Time.deltaTime * cameraSpeed);
            }
            //Keep track of distance for next loop
            previousDistance = distance;
            yield return null;
        }
    }
}
