using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float projectileVelocity = 10f;
    EnemyGuy enemyGuy;

    private void Start() {
        enemyGuy = GetComponent<EnemyGuy>();
        InvokeRepeating("Shoot", 3f, 3f);
    }

    public void Shoot()
    {
        if(enemyGuy.life > 0)
        {
            var instantiatedObj = (GameObject)Instantiate(bulletPrefab, new Vector2(transform.position.x , transform.position.y + -0.05f) , Quaternion.identity);
            int dir = transform.localScale.x > 0 ? 1 : -1;
            instantiatedObj.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileVelocity * dir, 0);
        }
        
    }
}
