﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class P3_UISaveSettings 
{
    public static bool bSound;
    public static bool bMusic;
    public static bool bVibration;
    public static bool bNotifications;

    public static void SavePlayerPrefs()
    {        
        PlayerPrefs.SetInt("sound", bSound == true ? 1:0);
        PlayerPrefs.SetInt("music", bMusic == true ? 1:0);
        PlayerPrefs.SetInt("vibration", bVibration == true ? 1:0);
        PlayerPrefs.SetInt("notifications", bNotifications == true ? 1:0);
    }
    public static void LoadPlayerPrefs()
    {
        if(PlayerPrefs.HasKey("music"))
        {
            bSound = PlayerPrefs.GetInt("sound") == 1 ? true : false;
            bMusic = PlayerPrefs.GetInt("music") == 1 ? true : false;
            bVibration = PlayerPrefs.GetInt("vibration") == 1 ? true : false;
            bNotifications = PlayerPrefs.GetInt("notifications") == 1 ? true : false;
        }
        else
        {
            bSound = true;
            bMusic = true;
            bVibration = true;
            bNotifications = true;
        }
    }
}
