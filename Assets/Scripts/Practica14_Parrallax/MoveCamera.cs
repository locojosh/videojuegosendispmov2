using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public float change = 10f;

    public void OnClick_Move(int dir)
    {
        transform.position += new Vector3(change * dir, 0f, 0f);
    }
}
