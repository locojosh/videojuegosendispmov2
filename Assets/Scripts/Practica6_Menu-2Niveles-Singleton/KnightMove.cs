﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(Rigidbody2D))]
public class KnightMove : MonoBehaviour
{
    public float velocity;
    Rigidbody2D rb;
    Animator animator;
    private float direction;
    public GyroControl gyroControl;

    private void Awake() 
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }
    private void Update() 
    {
        GyroMove();
        Move();
    }
    private void GyroMove()
    {
        if(gyroControl.gyroEulerAngles.y > 10) direction = 1;
        else if(gyroControl.gyroEulerAngles.y < -10) direction = -1;
    }
    private void Move()
    { 
        Vector2 newVelocity = new Vector2(direction*velocity, rb.velocity.y);
        
        rb.velocity = newVelocity;
        AudioManagerGO.Instance.PlayFootstep();
    }    
    private void Attack()
    {
        animator.SetTrigger("attack");
        AudioManagerGO.Instance.Play("attack");
    }
    //input system
    public void Move(InputAction.CallbackContext context)
    {
        direction = context.ReadValue<float>(); 
        if(direction != 0)
        {
            animator.SetBool("run", true);
            transform.localScale = direction > 0 ? new Vector3(1,1,1) : new Vector3(-1,1,1);
        }
        else
        {
            animator.SetBool("run", false);
        }
        //Move();
    }
    public void OnAttack(InputAction.CallbackContext context)
    {
        Attack();
    }
}
