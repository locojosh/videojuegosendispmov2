﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackKio : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    Animator anim;
    float nexFire = 0;

    public bool isInGame = false;
    public float firerate= 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isInGame) {
            if (Input.GetButtonDown("Fire1") && Time.time > nexFire)
            {
                nexFire = Time.time + firerate;
                anim.SetBool("attack", true);
                Invoke("Shoot", 0.2f);
            }

            if (Input.GetButtonUp("Fire1"))
            {
                anim.SetBool("attack", false);
            }            
        }       
    }

    void Shoot()
    {
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }
}

