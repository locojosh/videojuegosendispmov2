using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackShoot : MonoBehaviour
{
    public PoolManager projectilesPoolManager;
    public float projectileVelocity = 10f;
    public Transform shootPoint;

    float nextFire = 0;
    public float fireRate= 0.5f;
    private Animator anim; 

    void Start()
    {
        anim = GetComponent<Animator>();
    }
    public void OnTap_Shoot()
    {
        if(Time.time >= nextFire)
        {
            nextFire += fireRate;
            anim.SetBool("throw", true);
            Invoke("Shoot", 0.25f);
        }
        
    }
    private void Shoot()
    {
        GameObject newProjectile = projectilesPoolManager.RequestObject();
        newProjectile.transform.position = shootPoint.position;
        int dir = transform.localScale.x > 0 ? 1 : -1;
        newProjectile.GetComponent<Rigidbody2D>().velocity = new Vector2(projectileVelocity * dir, 0);
    }
}
