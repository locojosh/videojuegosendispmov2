﻿using UnityEngine;

[CreateAssetMenu(fileName = "Collectable", menuName = "Scriptables/Collectable", order = 0)]
public class Collectable_ScriptableObject : ScriptableObject 
{
    [SerializeField] private string type; //What type of collectable, ej. coins, gems, etc
    [SerializeField] private int number; //How many of that collectable

    public string Type {get {return type;}}
    public int Number {get{return number;}}

    public void AddToNumber()
    {
        number ++;
    }
    public void SetNumber(int value)
    {
        number = value;
    }
}