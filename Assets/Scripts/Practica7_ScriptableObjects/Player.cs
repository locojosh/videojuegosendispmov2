﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player Instance;

    private void Awake() 
    {
        Instance = this; 
    }
    private void Start() 
    {
        Load();
        MyGameManager.instance.Load();
    }
    public void UpdateUI()
    {
        UICollectables.Instance.UpdateUI(MyGameManager.instance.GetCollectable("coins").Number,
                                        MyGameManager.instance.GetCollectable("gems").Number);
    }
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.CompareTag("SignPoint"))
        {
            Save();
            MyGameManager.instance.Save();
        }
    }

    #region SAVE/LOAD
    public void Save()
    {
        GameSaveLoad.Save(PlayerPrefs.GetInt("match"), PlayerPrefs.GetInt("level"), "player", this);
    }
    public void Load()
    {
        GameData data = GameSaveLoad.Load(PlayerPrefs.GetInt("match"), PlayerPrefs.GetInt("level"), "player");

        if(data != null)
        {
            Vector3 position;
            position.x = data.position[0];
            position.y = data.position[1];
            position.z = data.position[2];
            transform.position = position;
        }        
    }
    #endregion
}
