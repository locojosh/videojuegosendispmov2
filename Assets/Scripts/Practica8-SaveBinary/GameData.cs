﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public float[] position = new float[3];
    public int[] collectablesNumber;

    public GameData(Player player)
    {
        position[0] = player.transform.position.x;
        position[1] = player.transform.position.y;
        position[2] = player.transform.position.z;
    }
    public GameData(MyGameManager theGameManager)
    {
        collectablesNumber = new int[theGameManager.Collectables.Length];

        for (int i = 0; i < collectablesNumber.Length; i++)
        {
            collectablesNumber[i] = theGameManager.Collectables[i].Number;
        }
    }
}
