using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamManAnimator : MonoBehaviour
{
    public Animator anim;
    public void  OnClick_SetBoolEating()
    {
        anim.SetBool("Eating", true);
    }
    public void OnClick_SetSmileTrigger()
    {
        anim.SetTrigger("SmileTrigger");
    }
}
